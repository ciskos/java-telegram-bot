package io.gitlab.ciskos.javatelegrambot;

import java.util.List;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.CopyMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class Bot extends TelegramLongPollingBot {

	private static final String BACK = "back";
	private static final String NEXT = "next";
	private static final String COMMAND_MENU = "/menu";
	private static final String COMMAND_WHISPER = "/whisper";
	private static final String COMMAND_SCREAM = "/scream";
	private Boolean screaming;
	private InlineKeyboardMarkup keyboardMarkup1;
	private InlineKeyboardMarkup keyboardMarkup2;

	@Override
	public void onUpdateReceived(Update update) {
		var message = update.getMessage();
		var messageId = message.getMessageId();
		var fromUser = message.getFrom();
		var fromId = fromUser.getId();
		var fromFirstName = fromUser.getFirstName();
		var text = message.getText();

//		System.out.println(fromId + " | " + fromFirstName + " написал " + text);
		System.out.println(update);

//		sendMessage(fromId, text);

		var next = InlineKeyboardButton.builder()
				.text("Next")
				.callbackData(NEXT)
				.build();
		var back = InlineKeyboardButton.builder()
				.text("Back")
				.callbackData(BACK)
				.build();
		var url = InlineKeyboardButton.builder()
				.text("Tutorial")
				.url("https://core.telegram.org/bots/api")
				.build();
		keyboardMarkup1 = InlineKeyboardMarkup.builder()
				.keyboardRow(List.of(next))
				.build();
		keyboardMarkup2 = InlineKeyboardMarkup.builder()
				.keyboardRow(List.of(back))
				.keyboardRow(List.of(url))
				.build();

		if (update.hasMessage()) {
//			if (message.isCommand()) {
//				if (COMMAND_SCREAM.equals(text)) {
//					screaming = true;
//				} else if (COMMAND_WHISPER.equals(text)) {
//					screaming = false;
//				} else if (COMMAND_MENU.equals(text)) {
//					sendMenu(fromId, "<b>Menu 1</b>", keyboardMarkup1);
//				}
//
//				return;
//			} 
			
			System.out.println("message");
		} else if (update.hasCallbackQuery()) {
			System.out.println("callback query");
		}
		
		if (screaming) {
			scream(fromId, message);
		} else {
			copyMessage(fromId, messageId);
		}

	}

	@Override
	public String getBotUsername() {
		return "TestBot";
	}

	@Override
	public String getBotToken() {
		// Тут должен вставляться токен. Без него бот не будет подключаться к системе.
		return "123456789";
	}

	public void sendMessage(Long userId, String message) {
		SendMessage sendMessage = SendMessage.builder().chatId(userId.toString()).text(message).build();

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			throw new RuntimeException(e);
		}
	}

	public void sendMenu(Long userId, String message, InlineKeyboardMarkup inlineKeyboardMarkup) {
		SendMessage sendMessage = SendMessage.builder()
				.chatId(userId.toString())
				.parseMode("HTML").text(message)
				.replyMarkup(inlineKeyboardMarkup)
				.build();
		
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void buttonTap(Long userId, String queryId, String data, int messageId) {
		EditMessageText newText = EditMessageText.builder()
				.chatId(userId.toString())
				.messageId(messageId)
				.text("")
				.build();
		EditMessageReplyMarkup newKeyboard = EditMessageReplyMarkup.builder()
				.chatId(userId.toString())
				.messageId(messageId)
				.build();
		
		if (NEXT.equals(data)) {
			newText.setText("Menu 2");
			newKeyboard.setReplyMarkup(keyboardMarkup2);
		} else if (BACK.equals(data)) {
			newText.setText("Menu 1");
			newKeyboard.setReplyMarkup(keyboardMarkup1);
		}
		
		AnswerCallbackQuery close = AnswerCallbackQuery.builder()
				.callbackQueryId(queryId)
				.build();
		
		try {
			execute(close);
			execute(newText);
			execute(newKeyboard);
		} catch (TelegramApiException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void copyMessage(Long userId, Integer messageId) {
		CopyMessage copyMessage = CopyMessage.builder().fromChatId(userId.toString()).chatId(userId.toString())
				.messageId(messageId).build();

		try {
			execute(copyMessage);
		} catch (TelegramApiException e) {
			throw new RuntimeException(e);
		}
	}

	private void scream(Long userId, Message message) {
		if (message.hasText()) {
			sendMessage(userId, message.getText().toUpperCase());
		} else {
			copyMessage(userId, message.getMessageId());
		}
	}

}
